package com.example.thibault.meeteat.ui.activityroom;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thibault.meeteat.R;

public class ActivityRoomFragmentUsers extends Fragment {

    public static ActivityRoomFragmentUsers newInstance() {
        return new ActivityRoomFragmentUsers();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_room_fragment_usericone, container, false);

        /* On affiche un user par defaut*/


        return view;
    }

    /**
     * Create a new user
     * @param pseudo
     * @return
     */
    User createUser(String pseudo){
        User user = new User(pseudo);
        return  user;
    }

    /**
     * Ask Server to know if new user in Room
     * @return
     */
    int askServerNewUser(){

        int newUser = 0;

        /* Requête qui interroge le serveur*/

        return newUser;
    }
}
