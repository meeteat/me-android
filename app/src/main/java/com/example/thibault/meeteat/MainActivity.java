package com.example.thibault.meeteat;

//imports android
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

//imports facebook
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

//imports google
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import com.example.thibault.meeteat.testServeur.TestActivity;

public class MainActivity extends AppCompatActivity {

    private Button roomButton;
    private Button osmButton;
    private Button testButton;

    private LoginButton facebookLogin;
    private SignInButton googleLogin;

    private CallbackManager callbackManager;

    private static int RC_SIGN_IN = 100;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialisation des composants Facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(this);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        //bouton de connexion Facebook
        //définitions des fonctions en utilisant l'API fournie par facebook
        setContentView(R.layout.activity_main);
        facebookLogin = findViewById(R.id.facebookLogin);
        facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            /**
             *
             * @param loginResult
             */
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent intent = new Intent(getApplicationContext(), MapBoxActivity.class);
                intent.putExtra("FacebookToken", loginResult.getAccessToken().getToken());
                startActivity(intent);
            }

            /**
             *
             */
            @Override
            public void onCancel() {
//                Toast.makeText(getApplicationContext(), "Login cancelled", Toast.LENGTH_LONG).show();
            }

            /**
             *
             * @param e
             */
            @Override
            public void onError(FacebookException e) {
//                Toast.makeText(getApplicationContext(), "Login failed for the following reason : " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        //Bouton de test pour accéder à la room sans connexion
        roomButton = findViewById(R.id.button);
        roomButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                OpenROOM();
            }
        });

        //logo de la page d'accueil
        ImageView homeImgFood =  findViewById(R.id.imageHomeFood);
        homeImgFood.setImageResource(R.drawable.imghomefood);

        //bouton de test permettant d'accéder à la map sans connexion
        osmButton = (Button) findViewById(R.id.osmButton);
        osmButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v){
                openOsmMap();
            }
        });

        //bouton de connexion avec Google
        //définition des fonctions en utilisant l'API fournie par Google
        googleLogin = findViewById(R.id.googleLogin);
        googleLogin.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        //bouton de test pour tester les requêtes HTTP
        testButton = findViewById(R.id.testButton);
        testButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                openTest();
            }
        });
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    /**
     *
     */
    private void OpenROOM() {
        Intent intent = new Intent(this, ActivityRoom.class);
        startActivity(intent);
    }

    /**
     *
     */
    private void openOsmMap() {
        Intent intent = new Intent(this, MapBoxActivity.class);
        startActivity(intent);
    }

    /**
     *
     */
    private void openTest() {
        Intent intent = new Intent(this, TestActivity.class);
        startActivity(intent);
    }

    /**
     *
     * @param completedTask
     */
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

//            Toast.makeText(getApplicationContext(), "User ID : " + account.getId() + ", Auth token : " + account.getIdToken(), Toast.LENGTH_LONG).show();
//            Log.i("googleToken", account.getIdToken());
            Intent intent = new Intent(this, MapBoxActivity.class);
            intent.putExtra("GoogleToken", account.getIdToken());
            startActivity(intent);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
//            Toast.makeText(getApplicationContext(), "Login failed for the following reason : " + e.getStatusCode(), Toast.LENGTH_LONG).show();
        }
    }
}
