package com.example.thibault.meeteat;

//imports android - java
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import java.util.List;

//imports mapbox
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;

import com.example.thibault.meeteat.ui.activityroom.ActivityRoomFragmentBottom;
import com.example.thibault.meeteat.ui.activityroom.ActivityRoomFragmentChatBox;
import com.example.thibault.meeteat.ui.activityroom.ActivityRoomFragmentUsers;

public class ActivityRoom extends AppCompatActivity implements PermissionsListener {

    private MapboxMap mapboxMap;
    private PermissionsManager permissionsManager;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        Mapbox.getInstance(this, getString(R.string.access_token));

        // Create supportMapFragment
        SupportMapFragment mapFragment;

        if (savedInstanceState == null) {

            /*
             * Mise en place du fragmentTransaction pour manipuler les fragments
             */
            FragmentTransaction transaction =
                    getSupportFragmentManager().beginTransaction();

            LatLng lyon = new LatLng(45.750000, 4.850000);

            // Build mapboxMap
            MapboxMapOptions options = new MapboxMapOptions();
            options.styleUrl(Style.OUTDOORS);
            options.camera(new CameraPosition.Builder()
                    .target(lyon)
                    .zoom(16)
                    .build());

            // Create map fragment
            mapFragment = SupportMapFragment.newInstance(options);

            /*
            *  Création des différents fragments dans la page ROOM
            */
            ActivityRoomFragmentBottom fragmentBottom = new ActivityRoomFragmentBottom();
            ActivityRoomFragmentUsers fragmentUser = new ActivityRoomFragmentUsers();
            ActivityRoomFragmentChatBox fragmentChatBox = new ActivityRoomFragmentChatBox();


            /*
             * Relie les fragments aux layouts de la room
             */

            // Add fragments to parent container
            transaction.add(R.id.firstLayoutRoom, mapFragment, "com.mapbox.map");
            transaction.add(R.id.secondLayoutRoom, fragmentChatBox);
            transaction.add(R.id.thirdLayoutRoom, fragmentUser);
            transaction.add(R.id.fourLayoutRoom, fragmentBottom);

            transaction.commit();

        } else {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("com.mapbox.map");
        }

        mapFragment.getMapAsync(mapboxMap -> {
            ActivityRoom.this.mapboxMap = mapboxMap;
            //Call of the function enabling the location of the user
            enableLocationComponent();
        });
    }

    /**
     *
     *
     */
    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationComponent() {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
        // Get an instance of the location component. Adding in LocationComponentOptions is also an optional
        // parameter
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.NORMAL);
        } else {
            permissionsManager = new PermissionsManager(this); 
            permissionsManager.requestLocationPermissions(this);
        }
    }

    /**
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     *
     *
     * @param permissionsToExplain
     */
    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    /**
     *
     * @param granted
     */
    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent();
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
