package com.example.thibault.meeteat;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

class IOAsyncTask extends AsyncTask<Location, Void, String> {


    private final OkHttpClient client = new OkHttpClient();

    /**
     *
     * @param params
     * @return
     */
    @Override
    protected String doInBackground(Location... params) {
        return sendData(params[0].getLatitude(), params[0].getLongitude());
    }

    /**
     *
     * @param latitude
     * @param longitude
     * @return
     */
    public String sendData(double latitude, double longitude) {
        try {

            RequestBody formBody = new FormBody.Builder()
                    .add("Latitude", Double.toString(latitude))
                    .add("Longitude", Double.toString(longitude))
                    .build();

            Request request = new Request.Builder()
                    .url("http://httpbin.org/post")
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            return "Error: " + e.getMessage();
        }
    }

    /**
     *
     * @param response
     */
    @Override
    protected void onPostExecute(String response) {
        Log.d("networking", response);
    }
}
