package com.example.thibault.meeteat.service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface CheckRoom {


    @Headers({
            "Content-type : application/json",
    })
    @GET
    Call<CheckRoom> checkRoom (@Header("authorization") String token,@Body boolean check);

}
