package com.example.thibault.meeteat.service;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Body;

public interface Search {


    @Headers({
            "Content-type : application/json",
    })
    @GET("api/Utilisateurs/join/{longitude}/{latitude}")
    Call<Search> createSearch(@Header("authorization") String token,@Header("plateforme") String platforme);

}
