package com.example.thibault.meeteat.ui.activityroom;

public class User {

    private String pseudo;

    public User(String s) {
        this.pseudo = s;
    }

    @Override
    public String toString() {
        return "User{" +
                "pseudo='" + pseudo + '\'' +
                '}';
    }
}
