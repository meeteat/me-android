package com.example.thibault.meeteat.ui.activityroom;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.thibault.meeteat.R;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.maps.MapboxMap;


public class ActivityRoomFragmentMaps extends Fragment {

    public static ActivityRoomFragmentMaps newInstance() {
        return new ActivityRoomFragmentMaps();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_room_fragment_maps, container, false);

    }
}
