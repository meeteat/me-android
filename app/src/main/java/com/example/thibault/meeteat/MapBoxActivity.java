package com.example.thibault.meeteat;

//imports android
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

//imports locaux
import com.example.thibault.meeteat.service.Search;

//imports Facebook
import com.facebook.login.LoginManager;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

//imports mapbox
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

//imports java
import java.util.List;

//imports retrofit
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Use the LocationComponent to easily add a device location "puck" to a Mapbox map.
 */
public class MapBoxActivity extends AppCompatActivity implements
        OnMapReadyCallback, PermissionsListener {

    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private MapView mapView;

    private double latitude, longitude;

    private FloatingActionButton btnSearch;
    private FloatingActionButton myLocationButton;
    private FloatingActionButton logoutButton;

    private GoogleApiClient mGoogleApiClient;

    private String token = null;
    private String platform = null;
    private String googleToken;
    private String facebookToken;


    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(this, getString(R.string.access_token));

        // This contains the MapView in XML and needs to be called after the access token is configured.
        setContentView(R.layout.activity_map_box);

        //bouton permettant de recentrer la vue sur la geolocalisation de l'utilisateur
        myLocationButton = (FloatingActionButton) findViewById(R.id.myLocationButton);
        myLocationButton.setOnClickListener(new View.OnClickListener() {

            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                enableLocationComponent();
            }
        });

        //bouton permettant à l'utilisateur de se déconnecter
        logoutButton = (FloatingActionButton) findViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                signOutFromGoogle();
                signOutFromFacebook();

            }
        });

        //création de la map
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        //récupération des tokens renvoyés par la MainActivity lors de la connexion avec Facebook ou Google
        googleToken = getIntent().getExtras().getString("GoogleToken");
        facebookToken = getIntent().getExtras().getString("FacebookToken");


        if (googleToken != null) {
            token = googleToken;
            platform = "google";
        }

        if (facebookToken != null) {
            token = facebookToken;
            platform = "facebook";
        }

        //bouton permettant de lancer la recherche de restaurant et de personne
        btnSearch = (FloatingActionButton) findViewById(R.id.searchButton);
        btnSearch.setOnClickListener(v -> {
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            latitude = locationComponent.getLastKnownLocation().getLatitude();
            longitude = locationComponent.getLastKnownLocation().getLongitude();

            String LATLNG = null;
//            Log.i(LATLNG, "Latitude : " + latitude + " / Longitude : " + longitude);

            /* passage à la room */
            Intent activityRoom = new Intent(MapBoxActivity.this, ActivityRoom.class);
            startActivity(activityRoom);

            /* REQUETE POUR LA RECHERCHE */
            String LaunchRequest = null;
//            Log.i(LaunchRequest, "LaunchRequest: ");
            //sendRequestSearch(token, platform,latitude,longitude);

        });
    }

    /**
     *
     * @param mapboxMap
     */
    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        MapBoxActivity.this.mapboxMap = mapboxMap;
        enableLocationComponent();
    }

    /**
     *
     */
    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent() {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            // Get an instance of the component
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

            // Activate
            locationComponent.activateLocationComponent(this);

            // Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

            // Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    /**
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     *
     * @param permissionsToExplain
     */
    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    /**
     *
     * @param granted
     */
    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent();
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /**
     *
     */
    @Override
    @SuppressWarnings({"MissingPermission"})
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
        mapView.onStart();
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     *
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     *
     */
    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    /**
     *
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     *
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /**
     *
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    /**
     *
     * @return
     */
    private boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(getApplicationContext()) != null;
    }

    /**
     *
     */
    private void signOutFromGoogle() {
        if (mGoogleApiClient.isConnected() && isSignedIn()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        /**
                         *
                         * @param status
                         */
                        @Override
                        public void onResult(Status status) {
                            Toast.makeText(getApplicationContext(), "Logged out from Google", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        }
                    });
        }
    }

    /**
     *
     */
    private void signOutFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            /**
             *
             * @param graphResponse
             */
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();
                Toast.makeText(getApplicationContext(), "Logged out from facebook", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

            }
        }).executeAsync();
    }

    /**
     *
     * @param token
     * @param platforme
     * @param latd
     * @param lgtd
     */
    private void sendRequestSearch(String token, String platforme, double latd, double lgtd) {

        String REQUEST = null;
//        Log.i(REQUEST, "sendRequestSearch: ");

        String url = "http://192.168.43.133:50423/";
//        Log.i(url, "url: ");

        //create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        //get client & call object for the request
        Search search = retrofit.create(Search.class);
        Call<Search> call = search.createSearch(token, platforme);

        //execute network request
        call.enqueue(new Callback<Search>() {
            /**
             *
             * @param call
             * @param response
             */
            @Override
            public void onResponse(Call<Search> call, retrofit2.Response<Search> response) {
//                Toast.makeText(MapBoxActivity.this, "SUCCESS :(", Toast.LENGTH_SHORT).show();
            }

            /**
             *
             * @param call
             * @param t
             */
            @Override
            public void onFailure(Call<Search> call, Throwable t) {
//                Toast.makeText(MapBoxActivity.this, "ERROR :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

