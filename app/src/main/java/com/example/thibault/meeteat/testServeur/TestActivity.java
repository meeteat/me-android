package com.example.thibault.meeteat.testServeur;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.thibault.meeteat.requestHTTP.AsyncHTTP;
import com.example.thibault.meeteat.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Button b = (Button)findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AsyncHTTP task = new AsyncHTTP(TestActivity.this);
                task.execute("https://openclassrooms.com/");
            }
        });
    }


}
