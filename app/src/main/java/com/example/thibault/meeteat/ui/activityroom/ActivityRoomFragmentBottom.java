package com.example.thibault.meeteat.ui.activityroom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thibault.meeteat.MapBoxActivity;
import com.example.thibault.meeteat.R;
import com.example.thibault.meeteat.service.CheckRoom;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ActivityRoomFragmentBottom extends Fragment {

    private TextView textRestaurant;
    private Button btnBackMap;
    private Button btnValidate;
    private String token = null;
    private String platform = null;
    private String googleToken;
    private String facebookToken;

    public static ActivityRoomFragmentBottom newInstance() {
        return new ActivityRoomFragmentBottom();
    }

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_room_fragment_bottom, container, false);

        textRestaurant = (TextView) v.findViewById(R.id.textRestaurant);


        setTextRestaurant();

        btnBackMap = v.findViewById(R.id.btnExitRoom);
        btnBackMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backMap();
            }
        });

        googleToken = getActivity().getIntent().getExtras().getString("GoogleToken");
        facebookToken = getActivity().getIntent().getExtras().getString("FacebookToken");


        if (googleToken != null) {
            token = googleToken;;
        }

        if (facebookToken != null) {
            token = facebookToken;
        }

        btnValidate = v.findViewById(R.id.btnValidateRoom);
        btnValidate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Toast.makeText(getActivity(), " J'attends des potes !!!", Toast.LENGTH_LONG).show();
                //checkValidateRoom(token);
            }

        });

        return v;
    }

    void setTextRestaurant() {
        textRestaurant.setText("EMPLACEMENT DETAILS RESTAURANT");
    }

    private void backMap() {
        Intent intent = new Intent(getActivity(), MapBoxActivity.class);
        startActivity(intent);
    }

    private void checkValidateRoom(String token) {

        String REQUEST = null;
//        Log.i(REQUEST, "checkValidateRoom: ");

        String url = "http://192.168.43.133:50423/";
//        Log.i(url, "url: ");

        //create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        //get client & call object for the request
        CheckRoom check = retrofit.create(CheckRoom.class);
        Call<CheckRoom> call = check.checkRoom(token,true);

        //execute network request
        call.enqueue(new Callback<CheckRoom>() {
            @Override
            public void onResponse(Call<CheckRoom> call, Response<CheckRoom> response) {

            }

            @Override
            public void onFailure(Call<CheckRoom> call, Throwable t) {

            }
        });

    }

}
